FROM psono-docker.jfrog.io/python:3.11.10-alpine3.20
LABEL maintainer="Sascha Pfeiffer <sascha.pfeiffer@psono.com>"

WORKDIR /usr/src/docker-image-updater

COPY app.py requirements.txt /usr/src/docker-image-updater/

RUN apk upgrade --no-cache && \
    pip3 install --upgrade pip && \
    pip3 install --no-cache-dir -r requirements.txt

CMD ["python3", "-u", "app.py"]
