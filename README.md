# PSONO Docker Image Updater




[![coverage report](https://gitlab.com/psono/psono-image-updater/badges/master/coverage.svg)](https://gitlab.com/psono/psono-image-updater/commits/master) [![build status](https://img.shields.io/docker/pulls/psono/psono-image-updater.svg)](https://hub.docker.com/r/psono/psono-image-updater/) [![Discord](https://img.shields.io/badge/Discord-join%20chat-738bd7.svg)](https://discord.gg/VmBMzTSbGV)


## Overview

If you work with docker and continuous integrations tools, you might need to update your images on your servers as soon as your build is finished.

This tool is a tiny webserver listening for a `POST` and automatically update the specified image using [Docker](https://docs.docker.com/engine/reference/api/docker_remote_api/) API.

You just have to run the image on your server, and configure your CI tool, in our case gitlab-ci.

## Installation

Launch the image on your server, where the images you want to update are
```
docker run -d \
  --name psono-image-updater \
  --restart=unless-stopped \
  --env TOKEN=abcd4242 \
  --env REGISTRY_USER=user \
  --env REGISTRY_PASSWD=pass \
  --env IMAGE_PREFIX=psono/psono- \
  -p 8080:8080 \
  -v /var/run/docker.sock:/var/run/docker.sock \
  psono/psono-image-updater
```

Available env variable:
```
TOKEN*
REGISTRY_USER
REGISTRY_PASSWD
IMAGE_PREFIX
REGISTRY_URL (default: https://index.docker.io/v1/)
HOST (default: 0.0.0.0)
PORT (default: 8080)
DEBUG (default: False)
```

\* mandatory variables. For `TOKEN` You can generate a random string, it's a security measure.

After, you just have to make a request to the server to update psono/psono-client:
```
POST http://ip-of-your-server/image/pull?token=abcd4242&restart_containers=true&image=client:latest
```

see examples below

IMAGE_PREFIX is an additional security measure to restrict images that can be spawned on the server

## Logs

You can access container logs with
```
docker logs --follow psono-image-updater
````


## Examples:

e.g. with curl:

```shell
curl -X POST 'http://ip-of-your-server:8080/image/pull?token=abcd4242&restart_containers=true&image=client:latest'
```

e.g. with Node.js

```javascript
var request = require('request');

var options = {
    url: 'http://ip-of-your-server:8080/image/pull?token=abcd4242&restart_containers=true&image=client:latest',
    method: 'POST'
};

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
    }
}

request(options, callback);
```

e.g. with PHP

```php
<?php
include('vendor/rmccue/requests/library/Requests.php');
Requests::register_autoloader();
$headers = array();
$response = Requests::post('http://ip-of-your-server:8080/image/pull?token=abcd4242&restart_containers=true&image=client:latest', $headers);
```

e.g. with Python

```python
import requests

params = (
    ('token', 'abcd4242'),
    ('restart_containers', 'true'),
    ('image', 'client:latest'),
)

response = requests.post('http://ip-of-your-server:8080/image/pull', params=params)
```

e.g. with Go

```go
response, error := http.Post("https://www.psono.pw/update/image/pull?token=abc&restart_containers=true&image=admin-client:latest", "", nil)
if error != nil {
	// handle error
}
defer response.Body.Close()
```

e.g. with Ruby

```ruby
require 'net/http'
require 'uri'

uri = URI.parse("http://ip-of-your-server:8080/image/pull?token=abcd4242&restart_containers=true&image=client:latest")
request = Net::HTTP::Post.new(uri)

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end
```

