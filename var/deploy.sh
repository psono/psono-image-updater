#!/usr/bin/env bash
apk upgrade --no-cache
apk add --update curl

docker pull registry.gitlab.com/psono/psono-image-updater:latest
docker tag registry.gitlab.com/psono/psono-image-updater:latest psono/psono-image-updater:latest
docker push psono/psono-image-updater:latest
